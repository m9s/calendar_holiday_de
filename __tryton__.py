# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Calendar Holiday Germany',
    'name_de_DE': 'Kalender Deutsche Feiertage (Allgemein)',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Provides calendar templates for country wide German holidays
    ''',
    'description_de_DE': '''
    - Stellt Kalendervorlagen für landesweit gültige deutsche Feiertage zur
      Verfügung.
    ''',
    'depends': [
        'calendar_template'
    ],
    'xml': [
        'holiday_de.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
